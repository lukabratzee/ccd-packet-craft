import sys
import time
import binascii
from binascii import hexlify, unhexlify
import codecs
import re

##################### D A T A ####################################


type_check = [
    [
        "00000000",
        "00000001",
        "00000081",
        "00000082",
        "00000090",
        "00000091",
        "00000092",
        "00000100",
        "00000101",
        "00000200",
        "00000201",
        "00000210",
        "00000300",
        "00000301",
        "00000302",
        "00000310",
        "00000400",
        "00000410",
        "00000800",
        "00000804",
        "00000810",
        "00000814",
        "000008F0",
        "00000900",
        "00000901",
        "00000902",
        "00000903",
        "00000911",
        "00000912",
        "00000920",
        "00000930",
        "00000940",
        "00000950",
        "00000960",
        "00000961",
        "00000962",
        "00000970",
        "00000971",
        "00000972",
        "00000973",
        "00000980",
        "00000981",
        "00000990",
        "00000991",
        "00000A00",
        "00000A01",
        "00000A02",
        "00000A08",
        "00000A10",
        "00000A11",
        "00000A12",
        "00000A18",
        "00000A80",
        "00000A81",
        "00000A82",
        "00000A83",
        "00000E00",
        "00000E01",
        "00000E10",
        "00000E11",
        "00000E20",
        "00000E21",
        "00000E30",
        "00000E31",
        "00000E40",
        "00000E41",
        "00000E50",
        "00000E51",
        "000FF100",
        "000FF101",
        "000FFF20",
        "000FFF60",
        "00100000",
        "00100800",
        "00100810",
        "00100820",
        "00100822",
        "00100830",
        "00100831",
        "00100832",
        "00100900",
        "00100980",
        "00101000",
        "00101010",
        "001010C0",
        "001010C1",
        "001010C2",
        "001010C3",
        "001010D0",
        "001010D1",
        "00110000",
        "00110001",
        "00110002",
        "00110003",
        "00110010",
        "00110100",
        "00110102",
        "00110110",
        "00110112",
        "00110220",
        "00110221",
        "00110222",
        "00110223",
        "00110400",
        "00110410",
        "00110500",
        "00110501",
        "00110502",
        "00110503",
        "00110510",
        "00120000",
        "00120010",
        "00180100",
        "00180101",
        "00180111",
        "00181100",
        "00181101",
        "00181111",
        "00183100",
        "00183101",
        "00183111",
        "001B0200",
        "001B0400",
        "001B0500",
        "001B0700",
        "00200000",
        "00200100",
        "00200110",
        "00200300",
        "00200310",
        "00280000",
        "00280001",
        "00280002",
        "00280012",
        "00284000",
        "00284001",
        "00284010",
        "00284011",
        "00300000",
        "00300001",
        "00300002",
        "00300003",
        "00300004",
        "00300005",
        "00300006",
        "00300007",
        "00300008",
        "00300010",
        "00300011",
        "00300012",
        "00310000",
        "00310001",
        "00310002",
        "00310003",
        "00310004",
        "00310005",
        "00310010",
        "00310011",
        "00310015",
        "00400000",
        "00400001",
        "00400002",
        "00500000",
        "00500001",
        "00500002",
        "00500003",
        "00500010",
        "00500090",
        "00500100",
        "00500101",
        "00500102",
        "00500103",
        "00500104",
        "00500110",
        "00500111",
        "00500112",
        "00500113",
        "00500114",
        "00500020",
        "00600000",
        "00600010",
        "00600000",
        "001C0000",
        "001D0000",
        "001D0001",
        "001D0002",
        "001D0003",
        "001D0004",
    ],
    [
        "Reset the device  ",
        "Reset the device and restore defaults ",
        "Get Recovery Status",
        "Get list of supported OBP commands  ",
        "Get main firmware revision ",
        "Get secondary firmware revision ",
        "Get main firmware subrevision ",
        "Get main serial number  ",
        "Get main serial number maximum length ",
        "Get device alias",
        "Get device alias maximum length",
        "Set device alias ",
        "Get user string count ",
        "Get length of each user string ",
        "Get user string ",
        "Set user string",
        "Get current time ",
        "Set current time  ",
        "Get RS2 Baud Rate ",
        "Get RS2 Flow Control State  ",
        "Set RS2 Baud Rate ",
        "Set RS2 Flow Control State ",
        "F0Save RS2 Settings  ",
        "Get Number of Network Interfaces ",
        "Get interface connection information ",
        "Is interface enabled ",
        "Run Interface Self Test ",
        "Save interface connection settings ",
        "Set interface enable  ",
        "Get GbE Enabled",
        "Set GbE Enabled",
        "Get MAC Address  ",
        "Set MAC Address    ",
        "Get WiFi SSID",
        "Get WiFi Mode",
        "Get WiFi Security",
        "Set WiFi SSID ",
        "Set WiFi Mode",
        "Set WiFi Security  ",
        "Set WiFi Passphrase ",
        "Get DHCP Server Enabled",
        "Get DHCP Server Address Range ",
        "Set DHCP Server Enabled",
        "Set DHCP Server Address Range  ",
        "Is DHCP Enabled ",
        "Get Number of IPv4 Addresses  ",
        "Get IPv4 Address (CIDR)  ",
        "Get IPv4 Default Gateway",
        "Set DHCP enable  ",
        "Delete Static IPv4 address",
        "Add Static IPv4 address (CIDR",
        "Set IPv4 Default Gateway ",
        "Is IPv4 Multicast Group Enabled ",
        "Get IPv4 Multicast Group Address ",
        "Get IPv4 Multicast Group Port ",
        "Get IPv4 Multicast Time to Live",
        "Get Original VID",
        "Get Original PID  ",
        "Get Original Manufacturer String  ",
        "Get Original Device String ",
        "Get VID ",
        "Get PID ",
        "Set VID  ",
        "Set PID ",
        "Get Manufacturer String  ",
        "Get Device String  ",
        "Set Manufacturer String  ",
        "Set Device String",
        "F0Get Payload of length N  ",
        "F1Echo Payload  ",
        "FFNew Chunked Primary Firmware Image  ",
        "FFNew Chunked FPGA Configuration ",
        "Abort acquisition  ",
        "Get Buffering Enabled  ",
        "Set Buffering Enabled ",
        "Get Maximum Buffer Size ",
        "Get Current Buffer Size ",
        "Clear All Buffered Spectra ",
        "Remove Oldest Spectra ",
        "Set Current Buffer Size ",
        "Get number of spectra in the buffer  ",
        "Get raw spectrum with meta data",
        "Get and send default spectrum immediately  ",
        "Get max ADC counts",
        "C0Get Offset Value (DAC Counts) ",
        "C1Get Reference Value (DAC Counts) ",
        "C2Get Offset Constraints ",
        "C3Get Reference Constraints",
        "D0Set Offset Value (DAC Counts) ",
        "D1Set Reference Value (DAC Counts)",
        "Get integration time (μs) ",
        "Get min integration time (μs)",
        "Get max integration time (μs)",
        "Get integration time stepsize(μs)",
        "Set integration time (μs)",
        "Get trigger mode   ",
        "Get number of backto-back spectra per trigger event ",
        "Set trigger mode ",
        "Set number of back-toback spectra per trigger event ",
        "Get number of pixels ",
        "Get active pixel ranges   ",
        "Get Optical Dark Pixel Ranges",
        "Get Electric Dark Pixel Ranges ",
        "Get lamp enable",
        "Set lamp enable ",
        "Get acquisition delay",
        "Get min acquisition delay ",
        "Get max acquisition delay",
        "Get acquisition delay step-size ",
        "Set acquisition delay ",
        "Get scans to average  ",
        "Set scans to average ",
        "Get wavelength coefficient count",
        "Get wavelength coefficient  ",
        "Set wavelength coefficient ",
        "Get nonlinearity coefficient count ",
        "Get nonlinearity coefficient ",
        "Set nonlinearity coefficient ",
        "Get stray light coefficient count",
        "Get stray light coefficient",
        "Set stray light coefficient ",
        "Get slit width ",
        "Get bench grating description",
        "Get bench filter description",
        "Get Detector Serial Number",
        "Get number of GPIO pins   ",
        "Get output enable vector ",
        "Set output enable vector ",
        "Get value vector ",
        "Set value vector    ",
        "Get number of eGPIO pins ",
        "Get eGPIO modes  ",
        "Get eGPIO mode ",
        "Set eGPIO mode ",
        "Get eGPIO Vector",
        "Get eGPIO Value ",
        "Set eGPIO Vector ",
        "Set eGPIO Value ",
        "Get single-strobe pulse delay (microseconds)",
        "Get single-strobe pulse width (microseconds)",
        "Get single-strobe enable ",
        "Get single-strobe minimum pulse delay (microseconds) ",
        "Get single-strobe maximum pulse delay (microseconds) ",
        "Get single-strobe pulse delay step size (microseconds) ",
        "Get single-strobe minimum pulse width (microseconds) ",
        "Get single-strobe maximum pulse width (microseconds) ",
        "Get single-strobe pulse width step size (microseconds) ",
        "Set single-strobe pulse delay (microseconds)  ",
        "Set single-strobe pulse width (microseconds) ",
        "Set single-strobe enable ",
        "Get continuous-strobe period",
        "Get continuous-strobe enable ",
        "Get continuous-strobe minimum period ",
        "Get continuous-strobe maximum period ",
        "Get continuous-strobe step size  ",
        "Get continuous-strobe width",
        "Set continuous-strobe period ",
        "Set continuous-strobe enable ",
        "Set continuous-strobe width",
        "Get Number of Temperature Sensors ",
        "Read Temperature Sensor ",
        "Read All Temperature Sensors ",
        "Get Number of SPI buses  ",
        "Get Number of SPI Chip Selects ",
        "Get Max Master Frequency",
        "Get Max Slave Frequency",
        "SPI full-duplex transfer",
        "Set SPI Clock Speed ",
        "Get SPI Bus Controller Mode ",
        "Get SPI Bus Transfer Mode ",
        "Get SPI Chip Select Polarity",
        "Get SPI Chip Select Delay",
        "Get SPI Sample Period",
        "Set SPI Bus Controller Mode",
        "Set SPI Bus Transfer Mode",
        "Set SPI Chip Select Polarity",
        "Set SPI Chip Select Delay ",
        "Set SPI Sample Period",
        "Save SPI Settings ",
        "Get Number of i buses",
        "I Bus Read  ",
        "I Bus Write",
        "Get Detector Type",
        "Get Battery Charge Level",
        "Get Battery (Dis)Charging Status",
        "Read Battery Temperature Sensor",
        "Read Battery Voltage   ",
        "Characterize Battery Depletion   ",
    ],
]

##################### C O L O U R ####################################


def prBlack(skk):
    return "\033[30m{}\033[00m".format(skk)


def prRed(skk):
    return "\033[31m{}\033[00m".format(skk)


def prGreen(skk):
    return "\033[32m{}\033[00m".format(skk)


def prYellow(skk):
    return "\033[33m{}\033[00m".format(skk)


def prBlue(skk):
    return "\033[34m{}\033[00m".format(skk)


def prMagenta(skk):
    return "\033[35m{}\033[00m".format(skk)


def prCyan(skk):
    return "\033[36m{}\033[00m".format(skk)


def prLightGray(skk):
    return "\033[37m{}\033[00m".format(skk)


def prDarkGrey(skk):
    return "\033[90m{}\033[00m".format(skk)


def prLightRed(skk):
    return "\033[91m{}\033[00m".format(skk)


def prLightGreen(skk):
    return "\033[92m{}\033[00m".format(skk)


def prLightYellow(skk):
    return "\033[93m{}\033[00m".format(skk)


def prLightBlue(skk):
    return "\033[94m{}\033[00m".format(skk)


def prLightMagenta(skk):
    return "\033[95m{}\033[00m".format(skk)


def prLightCyan(skk):
    return "\033[96m{}\033[00m".format(skk)


def prWhite(skk):
    return "\033[97m{}\033[00m".format(skk)


##################### D E C O D E ####################################

# Name: accept_hex_string
# Arguments: 1
# Descritpion: Takes passed variable and removes \x. Over sock, bytes get converted automatically, which shouldn't happen. This deals with that.
def accept_hex_string(xincoming_hex_string):

    # resolves ASCII issues in hex packets
    xincoming_hex_string = bytearray(xincoming_hex_string)
    xincoming_hex_string = "".join("{:02x}".format(x) for x in xincoming_hex_string)
    xincoming_hex_string = remove_symbol_two_byte(xincoming_hex_string)

    # previous requirement
    # xincoming_hex_string = str(xincoming_hex_string)
    # xincoming_hex_string = ''.join(str(e) for e in xincoming_hex_string)
    # xincoming_hex_string = xincoming_hex_string.split('\\x')[1:]

    return xincoming_hex_string


# Name: remove_symbol_two_byte
# Arguments: 1
# Descritpion: Strips passed variable of all symbols and resplits into two bytes
def remove_symbol_two_byte(xsymbol):

    xsymbol_stripped = re.sub(r"[^\w]", " ", xsymbol)
    xsymbol_stripped = re.findall("..", xsymbol_stripped)

    return xsymbol_stripped


# Name: to_bin
# Arguments: 1
# Descritpion: Converts passed argument to binary. Requires int or str
def to_bin(xflag):
    # converts hex to binary
    hexflag_bit = bin(int(xflag, 16))[2:]
    hexflag_bit = hexflag_bit[::-1]
    hexflag_bit = int(hexflag_bit, 2)
    return hexflag_bit


# Name: add_x
# Arguments: 1
# Descritpion: Adds \x to passed string. Useful for hex that expects the first byte to be \x
def add_x(xhex_file):
    # adds \x to hex string
    xhex_file = "\\x" + "\\x".join(
        xhex_file[n : n + 2] for n in range(0, len(xhex_file), 2)
    )
    # xhex_file = repr(xhex_file).translate(r'\\x')
    # xhex_file = unhexlify(xhex_file)

    return xhex_file


# Name: hex_to_denary_four_byte
# Arguments: 1
# Descritpion: Converts passed argument to denary. Requires 4 byte offset
def hex_to_denary_four_byte(xhex_to_value):
    # def - hex to denary conversion for payload message (4 byte number)
    # shifts bits to convert hex i.e 256^0, 256^1 etc
    bytes_remaining_dec = (
        (int(xhex_to_value[0], 16))
        | (int(xhex_to_value[1], 16) << 8)
        | (int(xhex_to_value[2], 16) << 16)
        | (int(xhex_to_value[3], 16) << 24)
    )

    return bytes_remaining_dec


# Name: hex_to_denary_four_byte_seg
# Arguments: 1
# Descritpion: Converts passed argument to str for payload messages. Requires 4 byte offset
def hex_to_denary_four_byte_seg(xhex_to_value):
    # def - hex to denary conversion for payload message (4 byte number)
    xhex_to_value.reverse()
    xhex_to_value = "".join(str(e) for e in xhex_to_value)
    xhex_to_value = xhex_to_value.upper()
    return xhex_to_value


# Name: hex_to_denary_bit
# Arguments: 1
# Descritpion: Converts passed argument to denary. Requires 2 byte offset
def hex_to_denary_bit(xhex_to_value):

    # strips non-alphanumeric characters (solved '!')
    xhex_to_value = re.sub(r"[^\w]", " ", xhex_to_value)
    bytes_remaining_dec = int(xhex_to_value, 16)

    return bytes_remaining_dec


# Name: flag_check
# Arguments: 1
# Descritpion: Takes passed variable and checks it against 2D array. Finds match, prints result
def flag_check(xflag_message):
    # def - checks bit value of flag
    messages = [
        [0, 1, 2, 3, 4, 5, 6],
        [
            "Response",
            "Achknowledgement",
            "Achknowledgement Requested",
            "Negative Acknowledgment",
            "Exception",
            "Deprecated Protocol",
            "Deprecated Message",
        ],
    ]
    # converts passed value into str
    xflag_message = "".join(str(e) for e in xflag_message)
    # recieves denary value from hex string
    flag_value = to_bin(xflag_message)
    # if element is found in value[0], print the element in the same position in value[1]
    if flag_value in messages[0]:
        flag_return1 = "Flag Message Reads: "
        flag_return2 = messages[1][messages[0].index(flag_value)]
        flag_return2 = "".join(str(e) for e in flag_return2 + "\r\n")
        flag_return = flag_return1 + flag_return2
        # print(
        #     prRed("Flag Message Reads: "),
        #     prGreen(messages[1][messages[0].index(flag_value)]),
        # )
        return flag_return


# Name: errorval_check
# Arguments: 1
# Descritpion: Takes passed variable and checks it against 2D array. Finds match, prints result
def errorval_check(xerrorval_check):

    value = [
        [
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            14,
            15,
            16,
            100,
            101,
            102,
            103,
            104,
            254,
            255,
        ],
        [
            "Success",
            "Bad Protocol",
            "Bad Message Type",
            "Bad Checksum",
            "Message Too Large",
            "Invalid Payload Length",
            "Device Not Ready",
            "Unknown Checksum Type",
            "Device Reset",
            "Too Many Buses",
            "Out of Memory",
            "Value Not Found",
            "Device Fault",
            "Bad Footer",
            "Request Interrupted",
            "I/O Error",
            "Bad Cipher",
            "Bad Firmware",
            "Incorrect Packet Length",
            "Wrong Hardware Revision",
            "Wrong Firmware Revision",
            "Proceed",
            "Deferred",
        ],
    ]

    # converts passed value into str
    xerrorval_check = "".join(str(e) for e in xerrorval_check)
    # recieves denary value from hex string
    hexflag_value = hex_to_denary_bit(xerrorval_check)

    # if element is found in value[0], print the element in the same position in value[1]
    if hexflag_value in value[0]:
        error_return1 = "Error Message Reads: "
        error_return2 = value[1][value[0].index(hexflag_value)]
        error_return2 = "".join(str(e) for e in error_return2 + "\r\n")
        error_return = error_return1 + error_return2
        # print(
        #     prRed("Error Message Reads: "),
        #     prGreen(value[1][value[0].index(hexflag_value)]),
        # )
        return error_return


# Name: payload_check
# Arguments: 1
# Descritpion: Takes passed variable and minuses 20 from result. This is to establish boundary checking
# to calculate the correct ending position of the segment i.e to read the contents of the message
def payload_check(xpayload_check):
    # passes hex string segment and performs bit shift conversion to denary
    bytes_remaining_dec = hex_to_denary_four_byte(xpayload_check[40:44])
    # minus 20 from the converted value to get the correct position from which to end reading
    bytes_remaining_dec = bytes_remaining_dec - 20

    if bytes_remaining_dec == 0:
        payload_return = "Payload is NULL"
        return payload_return
    else:
        payload_message = xpayload_check[44 : 44 + bytes_remaining_dec]
        payload_message = "".join(str(e) for e in payload_message)
        payload_return1 = "Payload Message reads: "
        payload_return2 = binascii.unhexlify(payload_message).decode()
        payload_return2 = "".join(str(e) for e in payload_return2 + "\r\n")
        payload_return = payload_return1 + payload_return2
        return payload_return


# Name: immediate_data_check
# Arguments: 1
# Descritpion: Takes passed variable and prints out result. Decodes hex into binary
def immediate_data_check(ximmediate_data_check):

    # value from hex string
    ximmediate_data_check = "".join(str(e) for e in ximmediate_data_check)
    ximmediate_data_check_ascii = binascii.unhexlify(ximmediate_data_check).decode()

    if ximmediate_data_check == "00000000000000000000000000000000":
        immediate_data_return = "Immediate Data is NULL" + "\r\n"
        return immediate_data_return

    elif ximmediate_data_check_ascii[0:4].isdigit():
        integration_time_return = ximmediate_data_check_ascii[0:4]
        immediate_data_return1 = "Immediate Data reads: "

        immediate_data_return2 = "".join(str(e) for e in integration_time_return)

        immediate_data_return = immediate_data_return1 + immediate_data_return2 + "\r\n"
        return immediate_data_return

    else:
        immediate_data_return1 = "Immediate Data reads: "
        immediate_data_return2 = binascii.unhexlify(ximmediate_data_check).decode()

        immediate_data_return2 = "".join(str(e) for e in immediate_data_return2)

        immediate_data_return = immediate_data_return1 + immediate_data_return2 + "\r\n"
        return immediate_data_return


# Name: message_type_check
# Arguments: 1
# Descritpion: Takes passed variable and checks it against 2D array. Finds match, prints result
def message_type_check(xmessage_type_check):
    # def - checks error table value and compares against hex value error code
    # converts passed value into usable format
    message_type_value = hex_to_denary_four_byte_seg(xmessage_type_check)
    # message_type_value = message_type_value.upper()

    # if element is found in value[0], print the element in the same position in value[1]
    if message_type_value in type_check[0]:
        # message_type_prefix = type_check[1][type_check[0].index(message_type_value)]
        return message_type_value


##################### E N C O D E ####################################


# Name: hex_check
# Arguments: 1
# Descritpion: Takes passed hex string and determines appropriate function to call
# Returns hex list
def hex_check(xprefix):

    hex_dict = {
        "00000100": get_serial,
        "00000E41": get_name,
        "00110000": get_integration_time,
        "00110010": set_integration_time,
    }

    message_type_value = message_type_check(xprefix[8:12])

    # try:
    #     xprefix = hex_dict[message_type_value]
    #     if xprefix == '00000100':
    #         get_serial()
    #         print(xprefix)
    #         return xprefix
    #     elif xprefix == '00000E41':
    #         get_name()
    #         print(xprefix)
    #         return xprefix
    #     elif xprefix == '00110000':
    #         set_integration()
    #         print(xprefix)
    #         return xprefix

    # except KeyError as e:
    #     print("Unknown message type")

    try:
        # get the message type from the request packet
        handler = hex_dict[message_type_value]
        # handler assumes name of hex_dict_message, which turns it into the required function
        response = handler(xprefix)
        return response

    except KeyError as e:
        print("Unknown message type")

    # # takes messaged type value to check for corrosponding message
    # print(message_type_check)
    # message_type_value = message_type_check(xprefix)
    # print(message_type_value)
    # if message_type_value == "00000100" in type_check[0]:
    #     xprefix = get_serial()
    #     # message_type_prefix = type_check[1][type_check[0].index(message_type_value)]
    #     # print(prRed("Prefix Message Reads: "), prGreen(message_type_prefix))
    #     return xprefix

    # elif message_type_value == "00000E41" in type_check[0]:
    #     xprefix = get_name()
    #     # message_type_prefix = type_check[1][type_check[0].index(message_type_value)]
    #     # print(prRed("Prefix Message Reads: "), prGreen(message_type_prefix))
    #     return xprefix

    # elif message_type_value == "00110000" in type_check[0]:
    #     xprefix = set_integration()
    #     # message_type_prefix = type_check[1][type_check[0].index(message_type_value)]
    #     # print(prRed("Prefix Message Reads: "), prGreen(message_type_prefix))
    #     return xprefix
    # TODO testing dictionary approach, works! Could refactor list to be a dict for readability + effiency

    # if message_type_value == "00000100" in hex_dict.keys():
    #     xprefix = get_serial()
    #     print(prRed("Prefix Message Reads: "), prGreen(hex_dict[message_type_value]))
    #     return xprefix
    # elif message_type_value == "00000E41" in hex_dict.keys():
    #     xprefix = get_name()
    #     print(prRed("Prefix Message Reads: "), prGreen(hex_dict[message_type_value]))
    #     return xprefix

    # if message_type_value == '000000100':

    #     xprefix = get_serial()
    #     return xprefix

    # elif message_type_value == '000000010':
    #     return xprefix


# Name: get_serial
# Arguments: 0
# Descritpion: gets serial of device


def get_serial(xprefix):

    # 53494D4A414B4521

    serial_name1 = "The serial of this device is: "
    serial_name2 = "SIMJAKE!"
    serial_name = serial_name1 + serial_name2 + "\r\n"

    # serial_hex = remove_symbol_two_byte(serial_hex)
    return serial_name


# Name: get_name
# Arguments: 0
# Descritpion: gets name of device
def get_name(xprefix):

    # 4F6365616E4658

    device_name1 = "The name of this device is: "
    device_name2 = "OceanFX"

    device_name = device_name1 + device_name2 + "\r\n"
    return device_name


global integration_time
integration_time = b"\x31\x30\x30\x30"

# Name: get_integration_time
# Arguments: 1
# Descritpion: reads global var and returns current value
# TODO get integrated without going through the rigmarol of creating/crafting
def get_integration_time(xprefix):

    global integration_time
    # decodes bytes to human-readable
    integration_time_response1 = (
        "Response reads: The current integration time is set to "
    )
    integration_time_response2 = integration_time.decode("utf-8")
    integration_time_response2 = "".join(str(e) for e in integration_time_response2)
    integration_time_response3 = "ms"

    integration_time_response = (
        integration_time_response1
        + integration_time_response2
        + integration_time_response3
        + "\r\n"
    )

    return integration_time_response


# Name: set_integration_time
# Arguments: 1
# Descritpion: sets integration time. Need to set variable through socket or use debug var (default)


def set_integration_time(xintegration_time):

    global integration_time
    xintegration_time = xintegration_time[24:28]
    new_integration_time = "".join(str(e) for e in xintegration_time)
    new_integration_time = bytearray.fromhex(new_integration_time)

    integration_time = new_integration_time
    new_integration_time = get_integration_time(integration_time)

    return new_integration_time


# Name: craft_integration
# Arguments: 1
# Descritpion: creates utf-8 hex of integration time so it can be used to craft packet


def craft_integration(xintegration_time):

    integration_time = xintegration_time
    integration_time_str = str(integration_time)
    integration_time_str = integration_time_str.encode("utf-8")
    integration_time_hex = integration_time_str.hex()

    return integration_time_hex
