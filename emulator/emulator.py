import socket
import sys, os
import binascii
import time

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))

from decoder import decoder_crafter

on_line = False

def send_data(xhex_request_return, xflag_return, xerrorval_return, ximmediate_data_return, xpayload_return):

    serversocket.sendall(xhex_request_return.encode())
    serversocket.sendall(xflag_return.encode())
    serversocket.sendall(xerrorval_return.encode())
    serversocket.sendall(ximmediate_data_return.encode())
    serversocket.sendall(xpayload_return.encode())

host = socket.gethostbyname(socket.gethostname())
# Symbolic name meaning all available interfaces
port = 12345  # Arbitrary non-privileged port
serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serversocket.bind((host, port))  # bind both host and port

on_line = True
print("server on:", host, ":", port)  # confirms host server
serversocket.listen(1)  # allows 1 failed server attempt
(serversocket, addr,) = serversocket.accept()  # passes accept values into conn and addr
# Was 256, now 64. Hex packet won't be huge
BUFFER_LEN = 64

while on_line:

    commands = serversocket.recv(BUFFER_LEN)

    if not commands:
        print("No commands found")
        break

    commands = decoder_crafter.accept_hex_string(commands)
    
    hex_request_return = decoder_crafter.hex_check(commands)
    # serversocket.sendall(hex_request_response.encode())
    # print("after hex", commands)
    # print(decoder_crafter.prMagenta("Header:"), commands[0:2])
    # print(decoder_crafter.prCyan("Protocol:"), commands[2:4])
    # print(decoder_crafter.prCyan("Flags:"), commands[4:6])
    flag_return = decoder_crafter.flag_check(commands[4:6])
    # print(flag_return)
    # serversocket.sendall(flag_return.encode())
    # print(decoder_crafter.prCyan("Error:"), commands[6:8])
    errorval_return = decoder_crafter.errorval_check(commands[6:8])
    # print(errorval_return)
    # serversocket.sendall(errorval_return.encode())
    # print(decoder_crafter.prCyan("Message Type:"), commands[8:12])
    decoder_crafter.message_type_check(commands[8:12])
    # print(decoder_crafter.prCyan("Regarding:"), commands[12:16])
    # print(decoder_crafter.prCyan("Reserved:"), commands[16:22])
    # print(decoder_crafter.prCyan("Checksum Type:"), commands[22:23])
    # print(decoder_crafter.prCyan("Immediate Data Length:"), commands[23:24])
    # print(decoder_crafter.prCyan("Immediate Data:"), commands[24:40])
    immediate_data_return = decoder_crafter.immediate_data_check(commands[24:40])

    # print(immediate_data_return)
    # serversocket.sendall(immediate_data_return.encode("utf-8"))
    time.sleep(1)
    # print(decoder_crafter.prCyan("Bytes Remaining:"), commands[40:44])
    payload_return = decoder_crafter.payload_check(commands)

    send_data(hex_request_return, flag_return, errorval_return, immediate_data_return, payload_return)
    
    # if bytes_remaining_dec == 0:
    #     serversocket.sendall("Payload is NULL".encode())
    #     time.sleep(1)

    #     # print(decoder_crafter.prCyan("Payload is"), decoder_crafter.prRed("NULL"))
    #     # print(decoder_crafter.prCyan("Checksum:"), commands[44:60])
    #     ### strips foreign symbols ###
    #     # print(decoder_crafter.prYellow("Footer:"), commands[60:64])
    #     # print("\r\n")

    # else:
    #     # if bytes remaining more than 20, apply converted value to the string to read from correct position
    #     # print(
    #     #     decoder_crafter.prCyan("Payload:"), commands[44 : 44 + bytes_remaining_dec]
    #     # )
    #     payload_message = commands[44 : 44 + bytes_remaining_dec]
    #     payload_message = "".join(str(e) for e in payload_message)
    #     payload_return1 = "Payload Message reads: "
    #     payload_return2 = binascii.unhexlify(payload_message).decode()
    #     payload_return2 = "".join(str(e) for e in payload_return2 + "\r\n")
    #     payload_return = payload_return1 + payload_return2
    #     serversocket.sendall(payload_return.encode())
    #     time.sleep(1)

        # print(
        #     decoder_crafter.prRed(payload_return1),
        #     decoder_crafter.prGreen(binascii.unhexlify(payload_message).decode()),
        # )

    # print(
    #     decoder_crafter.prCyan("Checksum:"),
    #     commands[44 + bytes_remaining_dec : 60 + bytes_remaining_dec],
    # )
    # ## strips foreign symbols ###
    # commands[60 + bytes_remaining_dec : 64 + bytes_remaining_dec] = commands[
    #     60 + bytes_remaining_dec : 64 + bytes_remaining_dec
    # ]
    # print(
    #     decoder_crafter.prYellow("Footer:"),
    #     commands[60 + bytes_remaining_dec : 64 + bytes_remaining_dec],
    # )

        # print("\r\n")

