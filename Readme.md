# Usage

## For the Local Conn:

 Compose file - run is -it i.e interactive
 `docker-compose run hex-ccd-conn`

## For the Emulator:

 Compose file -d runs in background
 `docker-compose up -d hex-ccd-emu`

 # docker-compose.yml

```
version: '3'
services:
  hex-ccd-emu:
    network_mode: host
    build:
      context: ./emulator
      dockerfile: Dockerfile

  hex-ccd-conn:
    network_mode: host
    build:
      context: ./local_conn
      dockerfile: Dockerfile
    stdin_open: true
    tty: true
```

