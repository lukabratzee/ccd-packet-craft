integration_time = 1000


def create_get_integration_time_response(packet):
    """
    packet:     OBP message (byte[])
    returns:    OBP message (byte[])
    """
    print("Building get integration time response...")
    # create the response based on the request packet
    # this is a VERY hacky way of building a response packet, would not recommend
    # start of the response packet is the same as the request
    response = packet[0:23]
    # set the immediate data length
    response += [0x04]
    # pop the integration time in the immediate data
    response += int(integration_time).to_bytes(4, byteorder="little", signed=False)
    # end of it is also identical because checksum is not used
    response += packet[28:]
    return response  # etc...


def create_get_serial_response(packet):
    return [0xC1, 0xC0, 0x00, 0x00]  # etc...


def create_demo_response(packet):
    return [0xFF, 0xFF, 0xFF, 0xFF]


message_handler_lookup = {
    0x00000E41: create_get_serial_response,
    0x00110000: create_get_integration_time_response,
    0x00000000: create_demo_response,
}
if __name__ == "__main__":
    # get integration time OBP message, essentially what will come through the socket
    packet = list(
        b"\xc1\xc0\x00\x00\x00\x00\x00\x00\x00\x00\x11\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x14\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xc5\xc4\xc3\xc2"
    )
    # do some initial validation on the packet header before trying to process
    try:
        print("Processing request")
        # get the message type from the request packet
        message_type = int.from_bytes(packet[8:12], byteorder="little", signed=False)
        handler = message_handler_lookup[message_type]
        response = handler(packet)
        print("Response packet:", response)
        # send response (if there is one...)
    except KeyError as e:
        print("Unknown message type")
