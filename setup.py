import os
import sys
from setuptools import setup

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

def readme():
    """print long description"""
    with open('Readme.md') as f:
        return f.read()


setup(
   name='ccd-packet-decoder',
   version='1.0',
   description='A hex encoder/decoder for CCD',
   long_description=readme(),
   author='Luke Clayton',
   author_email='luke@xors.com',
   packages=['ccd-packet-decoder'],  #same as name
)

# Runs in background [DEPRECATED]
# docker run -d --rm --network="host" --name hex-cdd-test1 hex-ccd-emu

# Compose file -d runs in background
# docker-compose -d up hex-ccd-emu
