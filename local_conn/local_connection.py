import socket
import sys, os
import time
from binascii import unhexlify

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
from decoder import decoder_crafter

global serversocket
global integration_time

command_sent = False
init_response_index = 0
hostc = socket.gethostbyname(socket.gethostname())


def send_set_integration(xtime):

    int_time_hex = decoder_crafter.craft_integration(xtime)
    set_hex = (
        "c1c000000000000010001100000000000000000000000004"
        + int_time_hex
        + "0000000000000000000000001400000000000000000000000000000000000000c5c4c3c2"
    )
    set_hex = str(set_hex)
    set_hex = unhexlify(set_hex)
    command_list[:] = [set_hex if x == command_list[3] else x for x in command_list]


command_list = [
    bytes.fromhex(
        "C1C000000300000000010000000000000000000000000000000000000000000000000000000000001400000000000000000000000000000000000000C5C4C3C2"
    ),  # 0 get serial
    bytes.fromhex(
        "C1C0000003000000410E0000000000000000000000000000000000000000000000000000000000001400000000000000000000000000000000000000C5C4C3C2"
    ),  # 1 get name
    bytes.fromhex(
        "C1C000000000000000001100000000000000000000000000000000000000000000000000000000001400000000000000000000000000000000000000C5C4C3C2"
    ),  # 2 get integration
    bytes.fromhex(
        "C1C000000000000010001100000000000000000000000004000000000000000000000000000000001400000000000000000000000000000000000000C5C4C3C2"
        # '\xc1\xc0\x00\x00\x03\x00\x00\x00A\x0e\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x031000\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x14\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xc5\xc4\xc3\xc2'
    ),  # 3 set integration
]

##################### M E N U ####################################


def operation():
    print("\r")
    time.sleep(1)
    print(decoder_crafter.prGreen("1. Get Serial"))
    print(decoder_crafter.prGreen("2. Get Name"))
    print(decoder_crafter.prGreen("3. Get Integration Time"))
    print(decoder_crafter.prGreen("4. Set Integration Time"))
    print(decoder_crafter.prRed("0. Quit"))
    print("\r")
    choice = input("Enter your choice: ")

    ### Convert string to type int ##
    choice = int(choice)

    if choice == 1:
        print(decoder_crafter.prCyan("Requesting Serial \n"))
        command = 0
        return command

    if choice == 2:
        print(decoder_crafter.prCyan("Requesting Name \n"))
        command = 1
        return command

    if choice == 3:
        print(decoder_crafter.prCyan("Get Integration Time \n"))
        command = 2
        return command

    if choice == 4:

        print(decoder_crafter.prCyan("Set Integration Time \n"))
        int_time = input("Enter time in ms: ")
        send_set_integration(int_time)
        command = 3
        return command

    if choice == 5:
        print(decoder_crafter.prCyan("DEBUG \n"))
        command = 4
        print(command_list[command])
        return command

    if choice == 0:
        print(decoder_crafter.prRed("Shutting Down"))
        print("\n")
        sys.exit()

    return choice


if __name__ == "__main__":
    # choice = menu()
    connected = False
    print(decoder_crafter.prBlue("    M A I N - M E N U    "))
    print(30 * "-")
    print(decoder_crafter.prGreen("Connecing to CCD..."))

    while not connected:
        try:
            portc = 12345  # The same port as used by the server
            serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            serversocket.connect((hostc, portc))
            connected = True
            BUFFER_LEN = 256

            print("Connected")

        except:
            pass

    while connected:
        if command_sent == False:
            command = operation()
            command_sent = True
            
        else:
            serversocket.sendall(command_list[command])

            command_str = serversocket.recv(BUFFER_LEN)

            print(decoder_crafter.prGreen(command_str.decode("utf-8")))

            # TODO accept multiple recv's before choosing next op, ending on Payload because I don't have a better solution for now
            # b is required for the byte string
            if b"Payload" in command_str:
                command_sent = False
                continue
            else:
                continue

