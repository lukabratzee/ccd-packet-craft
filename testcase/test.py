import pytest
import sys
import unittest

sys.path.insert(1, 'decoder/')

from decoder import decoder_crafter

# python -m unittest -v test.py


class test_cases(unittest.TestCase):

    # turns bytes into string (calls remove_symbol())
    def test_accept_hex_string(self):
        # this represents the hex string as received by the socket, converted automatically to ascii
        xincoming_hex_string = b"\xc1\xc0\x00\x00\x03\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x08SIMJAKE!\x00\x00\x00\x00\x00\x00\x00\x00\x14\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xc5\xc4\xc3\xc2"
        result = decoder_crafter.accept_hex_string(xincoming_hex_string)
        self.assertEqual(
            result,
            [
                "c1",
                "c0",
                "00",
                "00",
                "03",
                "00",
                "00",
                "00",
                "00",
                "01",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "08",
                "53",
                "49",
                "4d",
                "4a",
                "41",
                "4b",
                "45",
                "21",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "14",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "c5",
                "c4",
                "c3",
                "c2",
            ],
        )

    # turns string into list and removes foreign symbols
    def test_remove_symbol_two_bytes(self):
        xincoming_hex_string = "c1c00000030000000001000000000000000000000000000853494d4a414b452100000000000000001400000000000000000000000000000000000000c5c4c3c2"
        xincoming_hex_string = decoder_crafter.remove_symbol_two_byte(xincoming_hex_string)
        self.assertEqual(
            xincoming_hex_string,
            [
                "c1",
                "c0",
                "00",
                "00",
                "03",
                "00",
                "00",
                "00",
                "00",
                "01",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "08",
                "53",
                "49",
                "4d",
                "4a",
                "41",
                "4b",
                "45",
                "21",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "14",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "00",
                "c5",
                "c4",
                "c3",
                "c2",
            ],
        )

    # reveses and converts to str
    def test_hex_to_denary_four_byte_seg(self):
        xhex_to_value = ["00", "00", "01", "00"]
        result = decoder_crafter.hex_to_denary_four_byte_seg(xhex_to_value)
        self.assertEqual(result, "00010000")

    def test_to_bin(self):
        xto_bin = "0300"
        result = decoder_crafter.to_bin(xto_bin)
        self.assertEqual(result, 3)

    def test_add_x(self):
        xhex_file = "0300"
        result = decoder_crafter.add_x(xhex_file)
        self.assertEqual(result, "\\x03\\x00")

    def test_hex_to_denary_four_byte(self):
        xhex_den = ["14", "00", "00", "00"]
        result = decoder_crafter.hex_to_denary_four_byte(xhex_den)
        self.assertEqual(result, 20)

    def test_payload_check(self):
        xpayload_check = ["14", "00", "00", "00"]
        result = decoder_crafter.payload_check(xpayload_check)
        self.assertEqual(result, 0)

    def test_flag_check(self):
        xflag_message = ["03", "00"]
        result = decoder_crafter.flag_check(xflag_message)
        self.assertEqual(result, "Flag Message Reads: Negative Acknowledgment\r\n")

    def test_hex_to_denary_bit(self):
        xhex_den = "0000"
        result = decoder_crafter.hex_to_denary_bit(xhex_den)
        self.assertEqual(result, 0)

    def test_errorval_check(self):
        xerr_val = ["00", "01"]
        result = decoder_crafter.errorval_check(xerr_val)
        self.assertEqual(result, "Error Message Reads: Bad Protocol\r\n")

    def test_immediate_data_check(self):
        ximmNULL = "00000000000000000000000000000000"
        xim = "53494d4a414b4521"

        result1 = decoder_crafter.immediate_data_check(ximmNULL)  # null
        result2 = decoder_crafter.immediate_data_check(xim)  # serial

        self.assertEqual(result1, "Immediate Data is NULL")
        self.assertEqual(result2, "Immediate Data reads: SIMJAKE!\r\n")

    def test_message_type_check(self):
        xm_type = ["00", "01", "00", "00"]
        result = decoder_crafter.message_type_check(xm_type)
        self.assertEqual(result, "00000100")

    def test_hex_check(self):
        xhex_check = ["00", "01", "00", "00"]
        xhex_an = [
            "c1",
            "c0",
            "00",
            "00",
            "03",
            "00",
            "00",
            "00",
            "00",
            "01",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "08",
            "53",
            "49",
            "4d",
            "4a",
            "41",
            "4b",
            "45",
            "21",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "14",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "c5",
            "c4",
            "c3",
            "c2",
        ]
        result = decoder_crafter.hex_check(xhex_check)
        self.assertEqual(result, xhex_an)

    def test_get_serial(self):
        xhex_an = [
            "c1",
            "c0",
            "00",
            "00",
            "03",
            "00",
            "00",
            "00",
            "00",
            "01",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "08",
            "53",
            "49",
            "4d",
            "4a",
            "41",
            "4b",
            "45",
            "21",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "14",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "c5",
            "c4",
            "c3",
            "c2",
        ]
        result = decoder_crafter.get_serial()
        self.assertEqual(result, xhex_an)

    def test_get_name(self):
        xhex_an = [
            "C1",
            "C0",
            "00",
            "00",
            "03",
            "00",
            "00",
            "00",
            "41",
            "0E",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "07",
            "4F",
            "63",
            "65",
            "61",
            "6E",
            "46",
            "58",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "14",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "00",
            "C5",
            "C4",
            "C3",
            "C2",
        ]
        result = decoder_crafter.get_name()
        self.assertEqual(result, xhex_an)


if __name__ == "__main__":
    unittest.main()
